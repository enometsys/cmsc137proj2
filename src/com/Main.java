package com;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;
import java.util.StringTokenizer;

public class Main {
	private ServerSocket 	server;
	private String 			hash;
	private String 			line;
	private BufferedReader 	reader;
	
	public Main(int port){
		listen(port);
	}
	
	public void listen(int port){
		try {
			server = new ServerSocket(port);
			System.out.println("Listening for connection on port " + port + "...");
		    while (true){
		    	processRequest(server.accept());
		    }
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void processRequest(Socket clientSocket) throws IOException{
		InputStreamReader isr =  new InputStreamReader(clientSocket.getInputStream());
		DataOutputStream out = new DataOutputStream(clientSocket.getOutputStream());
		reader = new BufferedReader(isr);
        boolean post = false;
        
        line = reader.readLine(); //read first line (request)
		StringTokenizer st = new StringTokenizer(line);
		if(st.nextToken().equalsIgnoreCase("post")) post = true;
		String filename = st.nextToken();

		FileInputStream fstream = null;
		boolean params = true;
		boolean exists = true;
		
		if(filename.contains("?")){
			getValues(filename);
			filename = filename.replace("/","");
			String[] temp = filename.split("\\?");
			filename = temp[0];	
		} else {
			filename = filename.replace("/","");
			params = false;
		}
		
		try{
			fstream = new FileInputStream(filename);
		} catch(Exception e){
			exists = false;
			System.out.println(filename);
		}
		if(exists){
			String httpResponse = "HTTP/1.1 200 OK\r\n\r\n";
			out.write(httpResponse.getBytes("UTF-8"));
			if(post){
				System.out.println("POST");
				getPost();
				out.write(hash.getBytes("UTF-8"));
			} else System.out.println("GET");
			if(params) out.write(hash.getBytes("UTF-8"));
			sendBytes(fstream, out);
			fstream.close();
		}
        
        
        String line = reader.readLine();            
        while (!line.isEmpty()) {
            System.out.println(line);
            line = reader.readLine();
        }
        
        Date today = new Date();
        String httpResponse = "HTTP/1.1 200 OK\r\n\r\n" + today;
        clientSocket.getOutputStream().write(httpResponse.getBytes("UTF-8"));
	}
	
	public static void sendBytes(FileInputStream fstream, DataOutputStream out){ //send/render html file
		try {
			byte[] buffer = new byte[1024];
			int bytes = 0;
			while((bytes = fstream.read(buffer)) != -1){
				out.write(buffer, 0, bytes);
			}
		} catch(Exception e){}
	}


	public void getValues(String query){ //parse key-value pairs in GET query
		hash = "<table><tr>GET query KEY-VALUES:</tr>";

		String temp = query;
		temp = temp.replace("/", ""); //split get parameters on url
		String[] pairs = temp.split("\\?"); 
		pairs = pairs[1].split("&");
		for(int i=0; i<pairs.length; i+=1){
			String[] keyvalue = pairs[i].split("=");
			hash += "<tr><td>"+keyvalue[0]+"</td><td>"+keyvalue[1]+"</td></tr>"; //add key-values to html formatted table
		}
		hash += "</table>";
	}

	public void getPost(){
		try{
			hash = "<table><tr>Key-value pair(s)</tr>";

			line = reader.readLine();
				int contentLength = -1;
				while(!line.isEmpty()) {
					String temp[] = line.split(":"); //split the header
					if(temp[0].equals("Content-Length")) contentLength = Integer.parseInt(temp[1].trim()); //gets content length
					line = reader.readLine();
				}
	            String postParams = "";
	            // read the post data
	            if (contentLength > 0) {
	                char[] charArray = new char[contentLength]; //start reading at the end of the file to get post params
	                reader.read(charArray, 0, contentLength);
	                postParams = new String(charArray);
	                String[] keyvalue = postParams.replace("\n\r","").split("\n-+"); //split values based on postman format for post query
	                for(int i=0; i<keyvalue.length-1; i+=1){
	                	keyvalue[i] = keyvalue[i].replace("\r\n","");
	                	keyvalue[i] = keyvalue[i].substring(keyvalue[i].indexOf("\"")+1);
	                	String[] temp = keyvalue[i].split("\"");
	                	hash += "<tr><td>"+temp[0]+"</td><td>"+temp[1]+"</td></tr>"; //add key-values to html formatted table
	                }
				}
			hash += "</table>";
		} catch(Exception e){}
	}
}
